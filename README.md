# html-sandbox
Single html page with included: 
- Bootstrap
- Font Awesome
- jQuery 
- package.json for `npm`.

> Don't have npm? [Get it now](https://docs.npmjs.com/getting-started/installing-node)

## package.json
_dependencies_ contains:
- bootstrap

_devDependencies_ contains:
- `concurrently`- a utility to run multiple `npm` commands concurrently on OS/X, Windows, and Linux operating systems.
- `lite-server` - a light-weight, static file server, written and maintained by [John Papa](https://johnpapa.net)

> To install dependencies run in terminal `npm install`

List of `npm` scripts:
- `npm start` - runs the compiler and a server at the same time, both in "watch mode"